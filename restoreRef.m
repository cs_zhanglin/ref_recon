function [ ref,illum ] = restoreRef( hsi,rgb,crf,PCA_bases )
% ref: estimated high resolution hsi
% illum: estimated illumination
% hsi: simulated low resolution hsi
% rgb: simulated rgb
% crf: cemera response function

% --------------------Radiance restoration-------------------------
% 
[E,A]=SupResPALM(hsi,rgb,crf,8);
Rad_est=E*A;

% --------------------Illumination seperation on HSI------------------
dim=3;
[U,~,~] = svds(Rad_est,dim);

% QP with constraints
temp = -kron(PCA_bases(:,1:dim),ones(1,dim)).*kron(ones(1,dim),U(:,1:dim));
Quad = [eye(dim^2) temp.'; temp diag(sum(PCA_bases(:,1:dim).^2,2))];
temp = max(Rad_est,[],2);
[tempval,tempind] = max(temp);

band=size(hsi,1);
tempA = zeros(1,dim^2+band);
tempA(dim^2+tempind) = 1;

% invoke gruobi
para.outputflag = 0;
model.Q = sparse(Quad);
model.obj = zeros(1,band+dim^2);
model.lb = [-inf*ones(dim^2,1); temp];
model.A = sparse(tempA);
model.sense = '<';
model.rhs = tempval*1.1;

results  = gurobi(model,para);
xx = results.x;
Qt = reshape(xx(1:dim^2),dim,dim);
illum = xx(dim^2+1:end);

% Refinement
illum = LocalRefinement_GeneralLight_L2_Linear(illum, PCA_bases(:,1:dim), Rad_est);


ref=diag(1./illum)*Rad_est;
end

