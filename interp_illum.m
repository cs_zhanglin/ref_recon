function illum_to = interp_illum( illum_from,bands_from,bands_to )
% Interpolate the illumination to get the required bands

illum_to=interp1(bands_from,illum_from,bands_to)';
end

