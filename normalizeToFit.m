function normRef = normalizeToFit(Ref,prototype)

ins_proto=mean(prototype.^2,1).^0.5;
ins=mean(Ref.^2,1).^0.5;

scale=mean(ins_proto)/mean(ins);
normRef=Ref*scale;

