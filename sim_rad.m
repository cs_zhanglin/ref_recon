function rad = sim_rad( ref, illum )
% Knowing reflectance and illumination, simulate scene radiance.

% Normalize
ref=ref/max(ref(:));
illum=illum/max(illum(:));

rad=diag(illum)*ref;
end

