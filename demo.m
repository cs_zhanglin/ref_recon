% ---------------Path setting---------------------------------------------
addpath('illum_sep/');
addpath('include/');
addpath('sisal/');
addpath('reproject_simplex/');

datapath='F:\HSI\CAVE';
datafiles=dir(fullfile(datapath,'*.mat'));

% ----------------Load PCA Bases and standard illuminations---------------
% Load PCA Bases
[Macbeth,MacbethCCDC]=PrepareReflectanceData_Sample();
Bases=PCA_ZeroMean(Macbeth.');

% Load illumination
load('data/CIE_Standard_Illuminations_400_720.mat');


for ind=1:length(datafiles)
    % ------------------------Prepare simulated Data----------------------
    % Load data
    fullpath=fullfile(datapath,datafiles(ind).name);
    load(fullpath);
    truth=hyperConvert2d(truth);
    
    % Normalize
    truth=im2double(truth);
    truth=truth/max(truth(:));
    
    % Robust approximation - outlier removal
    W = ones(size(truth)); %no saturated pixels
    disp('- Augmented Lagrange Multiplier Method for Outlier Removal -');
    Data_n = RobustApproximation_M_UV_ALM(truth,W,8,1.2,1,1);
    
    % Simulate Reflectance that satisfied low-rank condition
    Ref = Bases(:,1:8)*(pinv(Bases(:,1:8))*Data_n);
    clear Data_n truth W Macbeth;
    
    % Simulate Radiance
    illum=interp_illum(illumD65,400:720,400:10:700);
    illum=illum/max(illum(:));
    Rad=sim_rad(Ref,illum);
    Rad=Rad/max(Rad(:));
    
    % Load cemera response function
    load('data/CRF_Nikon_D700.mat');
    
    % Simulate low resolution HSI and high resolution RGB
    [hyper,multi]=hyperSynthetic(Rad,crf,32);
    
    
    % -------------------Estimate Illumination and Reflectance------------ 
    % restore reflectance and illumination
    [Ref_est,illum_est]=restoreRef(hyper,multi,crf,Bases);
    illum_estn = illum_est*dot(illum_est,illum)/dot(illum_est,illum_est);
    
    % Normalize estimated reflectance
    Ref_est=normalizeToFit(Ref_est,Ref);
    
    
    % -------------------Save and show results----------------------------
    % Calculate estimation errors
    rmse(ind)=hyperErrRMSE(Ref,Ref_est);
    sam(ind)=hyperErrSam(Ref,Ref_est);
    lsam(ind)=hyperErrSam(illum,illum_estn);
    
    fprintf('Image No.%d: RMSE: %.2f, SAM: %.2f, SAM of Illumination: %.2f.\n',ind,rmse(ind),sam(ind),lsam(ind));
%     % show illumination restoration result
%     plot(illum);
%     hold on;
%     plot(illum_estn);
end


