function [Macbeth MacbethCCDC Munsell] = PrepareReflectanceData_Sample(Lb, Ub, interval)

%load data
load data/Macbeth_380_730_10

%from 420:700
Macbeth = Macbeth.';
Macbeth = Macbeth(3:end-3,:);


%load data
load data/MacbethCCDC_380_730_10


%from 420:700

MacbethCCDC = MacbethCCDC(3:end-3,:);



end
